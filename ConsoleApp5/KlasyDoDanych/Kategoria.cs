﻿using System.Xml.Serialization;

namespace ConsoleApp5
{
    public class Kategoria
    {
        private string nazwaKategorii;

        public Kategoria(string nazwaKategorii)
        {
            this.nazwaKategorii = nazwaKategorii;
        }
        
        public string NazwaKategorii { get { return this.nazwaKategorii; } }
    }
}