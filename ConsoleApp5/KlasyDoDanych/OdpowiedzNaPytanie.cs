﻿using System.Xml.Serialization;

namespace ConsoleApp5
{
    public class OdpowiedzNaPytanie
    {
        private string trescPytania;
        private string trescOdpowiedzi;
        private bool poprawnosc;

        public OdpowiedzNaPytanie(string trescPytania, string trescOdpowiedzi, bool poprawnosc)
        {
            this.trescPytania = trescPytania;
            this.trescOdpowiedzi = trescOdpowiedzi;
            this.poprawnosc = poprawnosc;
        }
        
        public string TrescPytania { get { return this.trescPytania; } }
        public string TrescOdpowiedzi { get { return this.trescOdpowiedzi; } }
        public bool Poprawnosc { get { return this.poprawnosc; } }
    }
}