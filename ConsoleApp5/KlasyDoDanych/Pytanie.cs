﻿using System;
using System.Collections.Generic;

namespace ConsoleApp5
{
    public class Pytanie
    {
        private Kategoria kategoria;
        private string trescPytania;
        private List<OdpowiedzNaPytanie> listaOdpowiedzi = new List<OdpowiedzNaPytanie>();

        public Pytanie(string trescPytania, Kategoria kategoria)
        {
            this.trescPytania = trescPytania;
            this.kategoria = kategoria;
        }

        public string TrescPytania { get { return trescPytania; } }
        public Kategoria Kategoria { get { return kategoria; } }

        // Metoda do dodawania odpowiedzi do pytania
        public void DodajOdpowiedzi()
        {
            Console.WriteLine("Podaj odpowiedzi, pierwsza podawana jest poprawna");

            // Latamy cztery razy w petli
            for (int i = 1; i <= 4; i++)
            {
                Console.WriteLine($"{i} odpowiedz:");
                string trescOdpowiedz = Console.ReadLine();

                // Ustawiamy docelowo poprawnosc na false
                bool poprawnosc = false;

                // Ustawiamy poprawnosc dla pierwszej odpowiedzi na true
                if (i == 1)
                    poprawnosc = true;

                listaOdpowiedzi.Add(new OdpowiedzNaPytanie(trescPytania, trescOdpowiedz, poprawnosc));

            }
        }

        // Wypisujemy dostepne odpowiedzi
        public void WypiszDostepneOdpowiedzi()
        {
            // Robmy nowa liste odpowiedzi
            List<OdpowiedzNaPytanie> nowalistaOdp = new List<OdpowiedzNaPytanie>();

            // Lista do wskaznikow
            List<int> dummyListOfInt = new List<int>() { 0, 1, 2, 3 };

            var rnd = new Random();

            // Mieszamy liste
            for (int i = 4; i > 0; i--)
            {
                // Losujemy nowa liczbe
                int nextRandomNumber = rnd.Next(0, i);

                // Dopisujemy element z naszej listy odpowiedz z wylosowanego miejsca do naszej nowej listy
                nowalistaOdp.Add(listaOdpowiedzi[dummyListOfInt[nextRandomNumber]]);

                // Usuwamy element z listy int z wylosowanego miejsca
                dummyListOfInt.RemoveAt(nextRandomNumber);
            }

            // Nadpisujemy stara liste odpowiedzi nasza nowa pomieszana lista
            listaOdpowiedzi = nowalistaOdp;

            for (int i = 0; i < listaOdpowiedzi.Count; i++)
            {
                Console.WriteLine($"[{i+1}] {listaOdpowiedzi[i].TrescOdpowiedzi}");
            }
        }

        // Zwracamy odpowiedz podana przez uzytkownika
        public OdpowiedzNaPytanie WybierzOdpowiedz()
        {
            Console.WriteLine("Podaj nr odpowiedzi");
            // Uzytkowk cos nam podaje
            string podanaWartosc = Console.ReadLine();

            // Deklarujemy zmienna typu int
            int konwertowanaPodanaWartosc;

            // sprawdzamy czy to co podal uzytwkownik jest liczba (intem), 
            // jezeli tak przypisujemy wynik przy pomocy "out"
            bool czyWartoscPoprawna = int.TryParse(podanaWartosc, out konwertowanaPodanaWartosc);

            // Sprawdzamy czy to co podal uzytkownik bylo intem,
            // jezeli nie to tworzymy nowa odpowiedz z taka sama trescia pytania i z false jako poprawna
            if (!czyWartoscPoprawna)
            {
                return new OdpowiedzNaPytanie(trescPytania, "BRAK", false);
            }

            if (konwertowanaPodanaWartosc < 0 && konwertowanaPodanaWartosc > 4)
            {
                return new OdpowiedzNaPytanie(trescPytania, "BRAK", false);
            }

            // Zwracamy odpowiedz podana przez uzytkownika
            return listaOdpowiedzi[konwertowanaPodanaWartosc - 1];
        }
    }
}