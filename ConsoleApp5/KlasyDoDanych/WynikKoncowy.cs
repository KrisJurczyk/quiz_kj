﻿using System.Collections.Generic;

namespace ConsoleApp5
{
    public class WynikKoncowy
    {
        private Kategoria kategoriaPytan;
        private List<OdpowiedzNaPytanie> odpowiedziGracza;
        private string punktacja;

        public WynikKoncowy(Kategoria kategoriaPytan, List<OdpowiedzNaPytanie> odpowiedziGracza)
        {
            this.kategoriaPytan = kategoriaPytan;
            this.odpowiedziGracza = odpowiedziGracza;

            int podanePoprawne = 0;
            int wszystkie = 0;

            foreach (var odpowiedz in odpowiedziGracza)
            {
                if (odpowiedz.Poprawnosc)
                    podanePoprawne++;
                wszystkie++;
            }

            punktacja = $"Wynik: {podanePoprawne} / {wszystkie}";
        }

        public Kategoria KategoriaPytan { get => kategoriaPytan; }

        public List<OdpowiedzNaPytanie> OdpowiedziGracza
        {
            get
            {
                var dummyList = new List<OdpowiedzNaPytanie>();
                dummyList.AddRange(odpowiedziGracza);
                return dummyList;
            }
        }

        public string Punktacja { get => punktacja; }
    }
}
