﻿using System;
using System.Collections.Generic;

namespace ConsoleApp5
{
    public class PrzygotowacRozgrywke
    {
        private List<Pytanie> listaPytanDoRozgrywki = new List<Pytanie>();

        public Rozgrywka PrzygotPytania()
        {
            bool jeszczeJednoPytanie = true;

            do
            {
                Console.WriteLine("Kategoria pytania:");

                // Podajemy kategorie pytania
                string kategoria = Console.ReadLine().ToUpper();

                // Towrzymy obiekt typu kategoria
                var nowaKategoria = new Kategoria(kategoria);

                Console.WriteLine("Tresc pytania:");

                // Podajemy tresc pytania
                string trescPytania = Console.ReadLine().ToUpper();

                // Tworzymy nowe pytanie
                Pytanie pyt = new Pytanie(trescPytania, nowaKategoria);

                // Dodajemy odpowiedzi do pytania
                pyt.DodajOdpowiedzi();

                // Dodajemy pytanie do listy
                listaPytanDoRozgrywki.Add(pyt);

                // Pytamy sie czy chcemy skonczyc dodawanie i przejsc dalej
                Console.WriteLine("Aby dodac nastepne pytanie nacisnij /t/");

                // Pytamy czy koniec dodawania pytan
                string p = Console.ReadLine().ToUpper();
                if (p != "T")
                    break;

            } while (jeszczeJednoPytanie);

            return new Rozgrywka(listaPytanDoRozgrywki);
        }
    }
}
