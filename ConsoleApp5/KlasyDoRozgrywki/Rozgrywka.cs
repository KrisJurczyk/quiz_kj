﻿using System;
using System.Collections.Generic;

namespace ConsoleApp5
{
    public class Rozgrywka
    {
        private List<Pytanie> pytania;

        // Pole kategorii, przypisujemy do niej wartosc w metodzie WybierzKategorie
        private Kategoria wybranaKategoria;

        public Rozgrywka(List<Pytanie> pytania)
        {
            this.pytania = pytania;
        }

        // Rozpoczynamy docelowa rozgrywke
        public WynikKoncowy RozpocznijRozgrywke()
        {
            // Step1: wywoujemy metode do wybrania kategorii
            WypiszDostepneKategorie();

            // Lista pytan z wybranej kategorii, narazie jest pusta
            List<Pytanie> listaPytanPodanejKategorii = new List<Pytanie>();

            // Iterujemy nasza liste pytan aby wziac tylko te z wybrana wczesniej kategoria
            foreach (var pojedynczePytanie in pytania)
            {
                // Jezeli pytanie z listy jest podanej kategorii, przypismujemy to pytanie do naszje listy
                if (pojedynczePytanie.Kategoria.NazwaKategorii == wybranaKategoria.NazwaKategorii)
                    listaPytanPodanejKategorii.Add(pojedynczePytanie);
            }

            // Tworzymy liste na odpowiedzi zeby gdzies je przechowywac
            List<OdpowiedzNaPytanie> listaOdpowiedzi = new List<OdpowiedzNaPytanie>();

            // Iterujemy po pytaniach z wybranej kategorii
            foreach (var pytanieZlisty in listaPytanPodanejKategorii)
            {
                Console.WriteLine(pytanieZlisty.TrescPytania);

                // Wypisujemy dostepne odpowiedzi, metoda znajduje sie w klasie Pytanie
                pytanieZlisty.WypiszDostepneOdpowiedzi();

                // Prosimy uzytkownika o wybranie odpowiedzi, metoda znajduje sie w klasie Pytanie
                listaOdpowiedzi.Add(pytanieZlisty.WybierzOdpowiedz());
            }

            int podanePoprawne = 0;
            int wszystkie = 0;
            Console.WriteLine("Score:");
            // Po prostu iterujey lise i wypisujemy co tam bylo
            foreach (var odpowiedz in listaOdpowiedzi)
            {
                Console.WriteLine($"{odpowiedz.TrescOdpowiedzi} {odpowiedz.Poprawnosc}");
                if (odpowiedz.Poprawnosc)
                    podanePoprawne++;
                wszystkie++;
            }

            Console.WriteLine($"Uzyskany wynik: {podanePoprawne}/{wszystkie}");

            return new WynikKoncowy(wybranaKategoria, listaOdpowiedzi);
        }

        // Medoda_Step1: Podajemy dostepne kategorie, pozniej wywolamy metode 
        private void WypiszDostepneKategorie()
        {
            Console.WriteLine("Dostepne kategorie:");

            // Nasza tymczasowa lista kategorii
            List<Kategoria> listaKategorii = new List<Kategoria>();

            // Iterujemy po liscie naszych pytan w celu uzyskania kategorii
            foreach (var pojedynczePytanie in pytania)
            {
                // Zawsze dodajemy pierwszy element do naszej listy kategorii
                if(listaKategorii.Count == 0)
                {
                    listaKategorii.Add(pojedynczePytanie.Kategoria);
                    continue;
                }

                // Iterujemy po naszej liscie kategorii
                foreach (var kategoria in listaKategorii)
                {
                    // Sprawdzamy czy w liscie kategorii juz istnieje taka kategoria
                    if (kategoria.NazwaKategorii == pojedynczePytanie.Kategoria.NazwaKategorii)
                        break;

                    // Jezeli nie to dodajemy kategorie do listy
                    listaKategorii.Add(pojedynczePytanie.Kategoria);
                    break;
                }
            }

            // Step2: Wywolujemy metode do wybrania kategorii
            WybierzKategorie(listaKategorii);
        }

        // Metoda_Step2: 
        private void WybierzKategorie(List<Kategoria> listaKategorii)
        {
            // Iterujemy po naszej gotowej liscie kategorii
            for (int i = 0; i < listaKategorii.Count; i++)
            {
                // Ladne wypisanie w konsoli naszych kategorii
                Console.WriteLine($"[{i + 1}] {listaKategorii[i].NazwaKategorii}");
            }

            Console.WriteLine("Podaj nr kategorii:");

            // Uzytkowk cos nam podaje
            string podanaWartosc = Console.ReadLine();

            // Deklarujemy zmienna typu int
            int konwertowanaPodanaWartosc;

            // sprawdzamy czy to co podal uzytwkownik jest liczba, 
            //jezeli tak przypisujemy wynik przy pomocy "out"
            bool czyWartoscPoprawna = int.TryParse(podanaWartosc, out konwertowanaPodanaWartosc);

            // Jezeli uzytkownik spierdolil misje, to wywolujemy metode jeszcze raz
            if (!czyWartoscPoprawna)
            {
                Console.WriteLine("Nie zostala podana zadna cyfra!");

                // Wywolujemy jeszce raz ta sama metode
                WybierzKategorie(listaKategorii);
            }

            // Uzytkownik podal wartosc ktora nie istnieje w zakresie naszej listy kategorii
            if (konwertowanaPodanaWartosc < 0 && konwertowanaPodanaWartosc > listaKategorii.Count)
            {
                Console.WriteLine("Nie ma takiej kategorii!");

                // Wywolujemy jeszce raz ta sama metode
                WybierzKategorie(listaKategorii);
            }

            //Przypisujemy do naszego pola kategorie z element z listy podany przez uzytkownika
            wybranaKategoria = listaKategorii[konwertowanaPodanaWartosc - 1];
        }
    }
}
