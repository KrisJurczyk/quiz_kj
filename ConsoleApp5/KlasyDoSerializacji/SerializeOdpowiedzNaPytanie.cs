﻿using System.Xml.Serialization;

namespace ConsoleApp5.KlasyDoSerializacji
{
    public class SerializeOdpowiedzNaPytanie
    {
        [XmlElement]
        public string TrescPytania;
        [XmlElement]
        public string UdzielonaOdpowiedz;
        [XmlElement]
        public bool PoprawnoscOdpowiedzi;
    }
}
