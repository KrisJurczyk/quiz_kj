﻿using ConsoleApp5.KlasyDoSerializacji;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ConsoleApp5
{
    [XmlRoot("UzyskaneWyniki")]
    public class SerializeWynikKoncowy
    {
        [XmlElement]
        public string KategoriaPytan;

        [XmlElement]
        public string Punktacja;

        [XmlArrayItem("Pytanie")]
        public List<SerializeOdpowiedzNaPytanie> OdpowiedziGracza = new List<SerializeOdpowiedzNaPytanie>();
    }
}
