﻿using System;
using System.Collections.Generic;

namespace ConsoleApp5.KlasyDoZapisu
{
    public class DostepneFormuZapisu
    {
        private List<ZapiszDo> dostepneOpcje = new List<ZapiszDo>();

        public DostepneFormuZapisu()
        {
            dostepneOpcje.Add(new ZapiszDoXML());
            dostepneOpcje.Add(new ZapiszDoJson());
        }

        public void WybierzFormeZapisu(WynikKoncowy wynik)
        {
            Console.WriteLine("Jak zapisac, wybierz numer");

            for (int i = 0; i < dostepneOpcje.Count; i++)
            {
                Console.WriteLine($"[{i + 1}]{dostepneOpcje[i].ToString()}");
            }

            string podanaWartosc = Console.ReadLine();

            int konwertowanaPodanaWartosc;

            bool czyWartoscPoprawna = int.TryParse(podanaWartosc, out konwertowanaPodanaWartosc);

            if (!czyWartoscPoprawna)
            {
                Console.WriteLine("Ok, nigdzie nie zapisze...");
                return;
            }

            if (konwertowanaPodanaWartosc < 0 && konwertowanaPodanaWartosc > dostepneOpcje.Count)
            {
                Console.WriteLine("Ok, nigdzie nie zapisze...");
                return;
            }

            dostepneOpcje[konwertowanaPodanaWartosc - 1].ZapiszWynik(wynik);
        }
    }
}
