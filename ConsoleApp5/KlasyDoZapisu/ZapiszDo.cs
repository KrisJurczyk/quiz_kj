﻿using ConsoleApp5.KlasyDoSerializacji;
using System.Collections.Generic;

namespace ConsoleApp5.KlasyDoZapisu
{
    public abstract class ZapiszDo
    {
        public void ZapiszWynik(WynikKoncowy odpoweidziDoZapisu)
        {
            ZapiszJako(WynikDoSerializacji(odpoweidziDoZapisu));
        }

        protected abstract void ZapiszJako(SerializeWynikKoncowy odpoweidziDoZapisu);

        private SerializeWynikKoncowy WynikDoSerializacji(WynikKoncowy odpoweidziDoZapisu)
        {
            var wkd = new SerializeWynikKoncowy();
            wkd.KategoriaPytan = odpoweidziDoZapisu.KategoriaPytan.NazwaKategorii;

            var listaOdpDoSer = new List<SerializeOdpowiedzNaPytanie>();

            foreach (var odpowiedz in odpoweidziDoZapisu.OdpowiedziGracza)
            {
                var odpDoSer = new SerializeOdpowiedzNaPytanie();

                odpDoSer.PoprawnoscOdpowiedzi = odpowiedz.Poprawnosc;
                odpDoSer.TrescPytania = odpowiedz.TrescPytania;
                odpDoSer.UdzielonaOdpowiedz = odpowiedz.TrescOdpowiedzi;

                listaOdpDoSer.Add(odpDoSer);
            }

            wkd.OdpowiedziGracza = listaOdpDoSer;
            wkd.Punktacja = odpoweidziDoZapisu.Punktacja;

            return wkd;
        }
    }
}
