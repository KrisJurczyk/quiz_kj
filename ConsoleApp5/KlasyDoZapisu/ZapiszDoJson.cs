﻿using System;
using System.Web.Script.Serialization;

namespace ConsoleApp5.KlasyDoZapisu
{
    public class ZapiszDoJson : ZapiszDo
    {
        public override string ToString()
        {
            return "JSON";
        }

        private string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

        protected override void ZapiszJako(SerializeWynikKoncowy odpoweidziDoZapisu)
        {
            var json = new JavaScriptSerializer().Serialize(odpoweidziDoZapisu);
            System.IO.File.WriteAllText($@"{path}\wynik.txt", json);
        }
    }
}
