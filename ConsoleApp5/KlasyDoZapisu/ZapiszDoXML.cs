﻿using ConsoleApp5.KlasyDoZapisu;
using System;
using System.IO;
using System.Xml.Serialization;

namespace ConsoleApp5
{
    public class ZapiszDoXML : ZapiszDo
    {
        private readonly string pathToFile;

        public ZapiszDoXML()
        {
            pathToFile = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        }

        public ZapiszDoXML(string pathToFile)
        {
            this.pathToFile = pathToFile;
        }

        public override string ToString()
        {
            return "XML";
        }

        protected override void ZapiszJako(SerializeWynikKoncowy odpoweidziDoZapisu)
        {
            XmlSerializer xs = new XmlSerializer(typeof(SerializeWynikKoncowy));
            TextWriter tw = new StreamWriter($@"{pathToFile}\wynik.xml");
            xs.Serialize(tw, odpoweidziDoZapisu);
        }

        
    }
}
