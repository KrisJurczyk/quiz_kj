﻿using ConsoleApp5.KlasyDoZapisu;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            // Tworzymy instancje klasy do przygotowania rozgrywki
            var pr = new PrzygotowacRozgrywke();

            // Wywolujemy metode do dodawania pytan
            var rozgrywka = pr.PrzygotPytania();

            // Wywolujemy metode do rozpoczecia rozgrywki
            var uzyskanyWynik = rozgrywka.RozpocznijRozgrywke();

            var zapis = new DostepneFormuZapisu();

            zapis.WybierzFormeZapisu(uzyskanyWynik);
        }
    }
}
